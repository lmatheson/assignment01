﻿--- Using this project ---

Prep - 
For Building or Editing: Ensure that Visual Studio, or another C# IDE is installed on your machine
For Running the Executable - Nothing

Steps - 
1. Clone the repository, or download the 'First Project' folder.
For Building or Editing:
 - Run the solution file in your IDE of choice
 - Build functionality can be ran from within the IDE in most cases

For Running the Executable:
 - Double click the executable file within the First Project folder found under the following directories:
	firstProgram\bin\Debug

--- What does the project do? ---
This project takes an input from the user, copies it and then outputs it.
